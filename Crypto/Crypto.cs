﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto {
    class Crypto {

        public static char rotx(char c, int b) {
            int result = c;
            if (c >= 'A' && c <= 'Z') {
                result -= 'A';
                result += b;
                result %= 26;
                if (result < 0) result += 26;
                result += 'A';
                
            }
            else if (c >= 'a' && c <= 'Z') {
                result -= 'a';
                result += b;
                result %= 26;
                if (result < 0) result += 26;
                result += 'a';
            }
            return (char)result;
        }

        public static string rotN(string message, int n) {
            StringBuilder sb = new StringBuilder(message);
            for (int i = 0; i < message.Length; ++i) {
                sb[i] = rotx(sb[i], n);
            }
            return sb.ToString();
        }

        public static string xor_encode(string message, string key) {
            StringBuilder sb = new StringBuilder(message);
            for (int i = 0; i < message.Length; ++i) {
                sb[i] ^= key[i % key.Length];
            }
            return sb.ToString();
        }

        public static string xor_decode(string message, string key) {
            return xor_encode(message, key);
        }

        public static string vignere_encode(string message, string key, string alphabet) {
            StringBuilder s = new StringBuilder(message);

            for (int i = 0; i < s.Length; i++) s[i] = Char.ToUpper(s[i]);
            key = key.ToUpper();
            int j = 0;
            for (int i = 0; i < s.Length; i++) {
                if (alphabet.Contains(s[i]))
                    s[i] = alphabet[(alphabet.IndexOf(s[i]) + alphabet.IndexOf(key[j])) % alphabet.Length];
                j = (j + 1) % key.Length;
            }

            return s.ToString();
        }

        public static string vignere_decode(string message, string key, string alphabet) {
            StringBuilder s = new StringBuilder(message);

            for (int i = 0; i < s.Length; i++) s[i] = Char.ToUpper(s[i]);
            key = key.ToUpper();
            int j = 0;
            for (int i = 0; i < s.Length; i++) {
                if (alphabet.Contains(s[i])) {
                    s[i] = alphabet[(alphabet.IndexOf(s[i]) - alphabet.IndexOf(key[j]) + alphabet.Length) % alphabet.Length];
                    j = (j + 1) % key.Length;
                }
            }

            return s.ToString();
        }

        static void Main(string[] args) {
            string english = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            Console.WriteLine(rotN("AZ", -27));
            Console.WriteLine(xor_encode(xor_encode("Goodbye", "Hello"), "Hello"));
            Console.WriteLine(vignere_decode(vignere_encode("Renaud", "Guillaume", english), "Guillaume", english));
            Console.ReadLine();
        }
    }
}
