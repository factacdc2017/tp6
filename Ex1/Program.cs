﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6_FIles {
    class Program {

        static String read_file(string filename) {
            if (filename == "")
                return "";

            String result = "";
            try {
                using (StreamReader sr = new StreamReader(filename)) { result = sr.ReadToEnd(); }
            }
            catch (Exception e) {
                Console.WriteLine("The file could not be read: {0}", e.Message);
            }

            return result;
        }

        static void write_to_file(string filename, string[] lines) {
            if (lines == null)
                return;

            using (StreamWriter file = new StreamWriter(filename)) {
                foreach (string line in lines)
                    file.WriteLine(line);
            }
        }

        static void Main(string[] args) {
            string[] lines = { "foo", "bar", "baz", "qux", "norf" };
            write_to_file("foo.txt", lines);
            write_to_file("foo.txt", null);
            Console.WriteLine(read_file("foo.txt"));
            Console.ReadLine();
        }
    }
}
