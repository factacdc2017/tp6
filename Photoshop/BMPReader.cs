﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace Photoshop {
    public class BMPReader: ICloneable {
        public int height { get; private set; }
        public int width  { get; private set; }

        private int pixel_array_offset;
        private short bits_per_pixel;

        private byte[] header;
        private Color[,] pixels;

        private BMPReader() {}

        public BMPReader(string filename) {
            FileStream fs = new FileStream(filename, FileMode.Open);

            if (!is_bitmap(fs))
                throw new Exception("Invalid bmp file");

            read_header(fs);
            read_pixels(fs);
            fs.Close();
        }

        public object Clone() {
            BMPReader b = new BMPReader();
            b.height = height;
            b.width = width;

            b.pixel_array_offset = pixel_array_offset;
            b.bits_per_pixel = bits_per_pixel;

            b.header = new byte[header.Length];
            b.pixels = new Color[height, width];

            Array.Copy(header, b.header, header.Length);
            Array.Copy(pixels, b.pixels, pixels.Length);

            return b;
        }

        public void save(string filename) {
            FileStream fs = new FileStream(filename, FileMode.Create);
            fs.Write(header, 0, header.Length);
            for (int y = 0; y < height; ++y) {
                int pos = 0;
                for (int x = 0; x < width; ++x) {
                    if (bits_per_pixel == 24)
                        write24(fs, pixels[y, x]);
                    else
                        write32(fs, pixels[y, x]);
                    pos += bits_per_pixel;
                }
                int move = 0;
                while (pos % 32 != 0) { pos++; move++; }

                for (int i = 0; i < move / 8; ++i)
                    fs.WriteByte(0);
            }

            fs.Close();
        }

        public void display_header() {
            Console.WriteLine("Width:           {0} pixels", width);
            Console.WriteLine("Height:          {0} pixels", height);
            Console.WriteLine("Offset:          {0} bytes", pixel_array_offset);
            Console.WriteLine("Bits per pixels: {0} bits", bits_per_pixel);
        }

        public Color get_pixel(int x, int y) {
            return pixels[y, x];
        }

        public void set_pixel(int x, int y, Color c) {
            pixels[y, x] = c;
        }

        public byte get_byte(int n) {
            int n_ = bits_per_pixel == 24 ? n / 3 : n / 4;
            int c = bits_per_pixel == 24 ? n % 3 : n % 4;
            int x = n_ % width;
            int y = n_ / width;

            if (c == 0)
                return pixels[y, x].B;
            else if (c == 1)
                return pixels[y, x].G;
            else if (c == 2)
                return pixels[y, x].R;
            return pixels[y, x].A;
        }

        public void set_byte(int n, byte b) {
            int n_ = bits_per_pixel == 24 ? n / 3 : n / 4;
            int c = bits_per_pixel == 24 ? n % 3 : n % 4;
            int x = n_ % width;
            int y = n_ / width;

            if (c == 0)
                pixels[y, x] = Color.FromArgb(pixels[y, x].A, pixels[y, x].R, pixels[y, x].G, b);
            else if (c == 1)
                pixels[y, x] = Color.FromArgb(pixels[y, x].A, pixels[y, x].R, b, pixels[y, x].B);
            else if (c == 2)
                pixels[y, x] = Color.FromArgb(pixels[y, x].A, b, pixels[y, x].G, pixels[y, x].B);
            else
                pixels[y, x] = Color.FromArgb(b, pixels[y, x].R, pixels[y, x].G, pixels[y, x].B);
        }

        private void read_header(FileStream fs) {
            fs.Seek(0xA, SeekOrigin.Begin);
            pixel_array_offset = read_int(fs);

            fs.Seek(4, SeekOrigin.Current);
            width = read_int(fs);
            height = read_int(fs);

            fs.Seek(2, SeekOrigin.Current);
            bits_per_pixel = read_short(fs);

            header = new byte[pixel_array_offset];
            fs.Seek(0, SeekOrigin.Begin);
            fs.Read(header, 0, pixel_array_offset);
        }

        private void read_pixels(FileStream fs) {
            pixels = new Color[height, width];

            for (int y = 0; y < height; ++y) {
                int pos = 0;
                for (int x = 0; x < width; ++x) {
                    // This can be optimized using delegates and doing
                    // this condition only once before the loop
                    if (bits_per_pixel == 24)
                        pixels[y, x] = read24(fs);
                    else
                        pixels[y, x] = read32(fs);

                    pos += bits_per_pixel;
                }
                int move = 0;
                while (pos % 32 != 0) { pos++; move++; }
                fs.Seek(move / 8, SeekOrigin.Current);
            }
        }

        private Color read24(FileStream fs) {
            int b = fs.ReadByte();
            int g = fs.ReadByte();
            int r = fs.ReadByte();
            return Color.FromArgb(255, r, g, b);
        }

        private Color read32(FileStream fs) {
            int b = fs.ReadByte();
            int g = fs.ReadByte();
            int r = fs.ReadByte();
            int a = fs.ReadByte();
            return Color.FromArgb(a, r, g, b);
        }

        private void write24(FileStream fs, Color c) {
            fs.WriteByte(c.B);
            fs.WriteByte(c.G);
            fs.WriteByte(c.R);
        }

        private void write32(FileStream fs, Color c) {
            fs.WriteByte(c.B);
            fs.WriteByte(c.G);
            fs.WriteByte(c.R);
            fs.WriteByte(c.A);
        }

        private bool is_bitmap(FileStream fs) {
            return fs.ReadByte() == 'B' && fs.ReadByte() == 'M';
        }

        private int read_int(FileStream fs) {
            byte[] val = new byte[4];
            fs.Read(val, 0, 4);

            return BitConverter.ToInt32(val, 0);
        }

        private short read_short(FileStream fs) {
            byte[] val = new byte[2];
            fs.Read(val, 0, 2);

            return BitConverter.ToInt16(val, 0);
        }
    }
}
