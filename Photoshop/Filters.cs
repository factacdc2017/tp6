﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Photoshop {
    public class Filters {
        static BMPReader to_grey(BMPReader image) {
            BMPReader clone = (BMPReader)image.Clone();

            for (int y = 0; y < clone.height; ++y)
                for (int x = 0; x < clone.width; ++x) {
                    Color c = clone.get_pixel(x, y);

                    double L = 0.3 * c.R + 0.59 * c.G + 0.11 * c.B;
                    Color greyed = Color.FromArgb(c.A, Convert.ToInt32(L), Convert.ToInt32(L), Convert.ToInt32(L));

                    clone.set_pixel(x, y, greyed);
                }

            return clone;
        }

        static BMPReader binarize(BMPReader image, int threshold) {
            BMPReader clone = (BMPReader)image.Clone();

            for (int y = 0; y < clone.height; ++y)
                for (int x = 0; x < clone.width; ++x) {
                    Color c = clone.get_pixel(x, y);

                    if ((c.R + c.B + c.G) / 3 > threshold)
                        clone.set_pixel(x, y, Color.FromArgb(255, 0, 0, 0));
                    else
                        clone.set_pixel(x, y, Color.FromArgb(255, 255, 255, 255));
                }

            return clone;
        }

        static bool is_in_array(int x, int y, int width, int height) {
            return !(x < 0 || y < 0 || x >= width || y >= height);
        }

        static int clamp(int min, int max, int x) {
            if (x < min) return min;
            if (x >= max) return max;
            return x;
        }

        static Color do_work(BMPReader img, int x, int y, float[,] mat) {
            int r = 0, g = 0, b = 0;
            for (int j = -1; j <= 1; ++j)
                for (int i = -1; i <= 1; ++i)
                    if (is_in_array(x + i, y + j, img.width, img.height)) {
                        Color c = img.get_pixel(x + i, y + j);
                        r += (int)(c.R * mat[i + 1, j + 1]);
                        g += (int)(c.G * mat[i + 1, j + 1]);
                        b += (int)(c.B * mat[i + 1, j + 1]);
                    }
            Color ret = Color.FromArgb(255, clamp(0, 255, r / 3), clamp(0, 255, g / 3), clamp(0, 255, b / 3));
            return ret;
        }

        public static BMPReader convolution(BMPReader image, float[,] mat) {
            BMPReader clone = (BMPReader)image.Clone();

            for (int y = 0; y < clone.height; ++y)
                for (int x = 0; x < clone.width; ++x)
                    clone.set_pixel(x, y, do_work(clone, x, y, mat));

            return clone;
        }

    }
}
