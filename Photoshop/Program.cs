﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Photoshop {
    public class Program {

        static void Main(string[] args) {

            BMPReader bt = new BMPReader("./flag.bmp");
            bt.display_header();

            Stegano s = new Stegano(bt);
            s.stegano_hide("Hello my name is Renaud !");
            Console.WriteLine(s.stegano_find());
            Console.ReadLine();

            bt.save("../../little_2.bmp");

        }
    }
}
