﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Photoshop {
    class Stegano {
        public BMPReader img { get; private set; }

        public Stegano(BMPReader img) {
            this.img = img;
        }

        // @return: if the bit was hidden or not
        public bool hide_bit(bool bit, int position) {
            byte b = img.get_byte(position);
            b = (byte)(b & 0xFE);

            b = (byte)(b | (bit ? 1 : 0));
            img.set_byte(position, b);
            return true;
        }

        // @return: -1 if not bit is hidden her, else the value of the bit
        public int read_bit(int position) {
            byte b = img.get_byte(position);
            b = (byte)(b & 1);

            return b;
        }

        public void stegano_hide(string message) {
            // insert message size in message.
            message = message.Length.ToString().PadLeft(4, '0') + message;
            int c = 0;
            int b = 0;

            for (int i = 0; i < img.height * img.width; ++i) {
                int mask = 1 << b;
                bool bit = ((message[c] & mask) >> b) == 1 ? true : false;

                if (hide_bit(bit, i))
                    b++;

                if (b == 8) { b = 0; c++; }
                if (c == message.Length) break;
            }
        }

        public string stegano_find() {
            String size = "0000";
            StringBuilder sb = new StringBuilder(size);
            for (int i = 0; i < 4 * 8; ++i) { // Read size
                int b = read_bit(i);

                sb[i / 8] = b == 1 ? set_bit(sb[i / 8], i % 8) : clear_bit(sb[i / 8], i % 8);
            }

            int len = Int32.Parse(sb.ToString());
            string message = new string((char)0, len);
            sb = new StringBuilder(message);

            int n = 0;
            for (int i = 4 * 8; i < 4 * 8 + len * 8; ++i) {
                int b = read_bit(i);

                sb[n / 8] = b == 1 ? set_bit(sb[n / 8], n % 8) : clear_bit(sb[n / 8], n % 8);
                ++n;
            }

            return sb.ToString();
        }

        private char set_bit(char x, int n) {
            return (char)(x | (1 << n));
        }

        private char clear_bit(char x, int n) {
            return (char)(x & ~(1 << n));
        }


    }
}
