﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    [Fact.Attribute.Test.Group("Crypto")]
    class CryptoTester {
        private Finder f;

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "Crypto");
        }

        [Fact.Attribute.Test.Test("RotN")]
        public void rotN() {
            System.Reflection.MethodInfo m = f.get("rotN");

            for (int i = -26; i <= 26; ++i) {
                string randString = RandomString(32);
                String got = (String) m.Invoke(null, new Object[] { randString, i });
                String expected = Crypto.Crypto.rotN(randString, i);

                Fact.Assert.Basic.AreEqual(expected, got, Fact.Assert.Assert.Trigger.FATAL,
                    "Error when using RotN cypher on " + randString + " with rotation of " + i);
            }
        }

        [Fact.Attribute.Test.Test("Xor Encode")]
        public void xor_encode_tester() {
            System.Reflection.MethodInfo m = f.get("xor_encode");

            for (int i = 0; i <= 26; ++i) {
                string randString = RandomString(32);
                string randkey = RandomString(32);

                String got = (String) m.Invoke(null, new Object[] { randString, randkey });
                String expected = Crypto.Crypto.xor_encode(randString, randkey);

                Fact.Assert.Basic.AreEqual(expected, got, Fact.Assert.Assert.Trigger.FATAL,
                    "Error when using xor_encode cypher on " + randString + " with key of " + randkey);
            }
        }

        [Fact.Attribute.Test.Test("Xor Decode")]
        public void xor_decode() {
            System.Reflection.MethodInfo m = f.get("xor_decode");

            for (int i = 0; i <= 26; ++i)
            {
                string randString = RandomString(32);
                string randkey = RandomString(32);

                String got = (String)m.Invoke(null, new Object[] { randString, randkey });
                String expected = Crypto.Crypto.xor_decode(randString, randkey);

                Fact.Assert.Basic.AreEqual(expected, got, Fact.Assert.Assert.Trigger.FATAL,
                    "Error when using xor_decode cypher on " + randString + " with key of " + randkey);
            }
        }

        [Fact.Attribute.Test.Test("Vignere Encode")]
        public void vignere_encode() {
            System.Reflection.MethodInfo m = f.get("vignere_encode");
            string english = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            for (int i = 0; i <= 26; ++i)
            {
                string randString = RandomString(32);
                string randkey = RandomString(32);

                String got = (String)m.Invoke(null, new Object[] { randString, randkey });
                String expected = Crypto.Crypto.vignere_encode(randString, randkey, english);

                Fact.Assert.Basic.AreEqual(expected, got, Fact.Assert.Assert.Trigger.FATAL,
                    "Error when using vignere_encode cypher on " + randString + " with key of " + randkey);
            }
        }

        [Fact.Attribute.Test.Test("Vignere Decode")]
        public void vignere_decode() {
            System.Reflection.MethodInfo m = f.get("vignere_decode");
            string english = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            for (int i = 0; i <= 26; ++i)
            {
                string randString = RandomString(32);
                string randkey = RandomString(32);

                String got = (String)m.Invoke(null, new Object[] { randString, randkey });
                String expected = Crypto.Crypto.vignere_decode(randString, randkey, english);

                Fact.Assert.Basic.AreEqual(expected, got, Fact.Assert.Assert.Trigger.FATAL,
                    "Error when using vignere_encode cypher on " + randString + " with key of " + randkey);
            }
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown() {
            f.Cleanup();
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

    }
}
