﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TP6_EX1;

namespace Tester
{
    [Fact.Attribute.Test.Group("Ex0")]
    class Ex0Tester {
        private Finder f;

        private String ref_read_file = "/read.txt";
        private String ref_write_file = "/write.txt";

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "Ex1");
        }

        [Fact.Attribute.Test.Test("Read")]
        public void read_file_tester() {

            string[] lines = { "foo", "bar", "baz", "qux", "norf" };
            Program.write_to_file(f._TempFolder + ref_read_file, lines);

            String got = (String) f.get("read_file")
                .Invoke(obj: null, parameters: new Object[] { f._TempFolder + ref_read_file });

            String reference = Program.read_file(f._TempFolder + ref_read_file);

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(reference, "", 0),
                new Fact.Runtime.Process.Result(got, "", 0));

            diff.FormatTrim = true;
            diff.FormatEndOfLine = true;
            diff.FormatSpace = true;

            Fact.Assert.Misc.ExpectTestPass(diff.Run(0));
        }

        [Fact.Attribute.Test.Test("Write")]
        public void write_file_tester() {

            string student_test_file = "/write_student.txt";

            string[] lines = { "foo", "bar", "baz", "qux", "norf" };
            Program.write_to_file(f._TempFolder + ref_write_file, lines);

            f.get("write_to_file").Invoke(obj: null, parameters: new Object[] { f._TempFolder + student_test_file, lines });

            String reference = Program.read_file(f._TempFolder + ref_write_file);
            String test = Program.read_file(f._TempFolder + ref_read_file);

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(reference, "", 0),
                new Fact.Runtime.Process.Result(test, "", 0));

            diff.FormatTrim = true;
            diff.FormatEndOfLine = true;
            diff.FormatSpace = true;

            Fact.Assert.Misc.ExpectTestPass(diff.Run(0));
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown() {
            f.Cleanup();
        }

    }
}
