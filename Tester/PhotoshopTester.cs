﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using Photoshop;
using System.IO;
using System.Reflection;
using Tester.Tools;

namespace Tester
{
    [Fact.Attribute.Test.Group("Photoshop")]
    public class PhotoshopTester
    {
        private Finder f;
        private Tools.Tools t;
        private System.Drawing.Bitmap flag;

        private string src_header = "/header.bmp";
        private string src_save = "/save.bmp";
        private string src_bin = "/bin.bmp";
        private string src_grey = "/grey.bmp";
        private string src_conv = "/conv.bmp";

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "Photoshop");
            this.create_bitmap();
            t = new Tools.Tools(f);
        }

        [Fact.Attribute.Test.Test("BmpReader: Header")]
        public void readHeader() {
            string expected = readHeader_ref();

            // Get instance

            object instance = t.Instance("BMPReader", new object[] { f._TempFolder + src_header });

            ConsoleCatcher c = new ConsoleCatcher(true);
            f.get("display_header").Invoke(obj: instance, parameters: new Object[] { });
            string result = c.Release();


            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(expected, "", 0),
                new Fact.Runtime.Process.Result(result, "", 0));

            diff.FormatTrim = true;
            diff.FormatEndOfLine = true;
            diff.FormatSpace = true;

            Fact.Assert.Misc.ExpectTestPass(diff.Run(0));
        }

        [Fact.Attribute.Test.Test("BmpReader: Write")]
        public void write_BMP() {

            string dst = "./flag_cpy.bmp";

            Type t = f.get_class("BMPReader");
            object instance = f.get_ctor(t, typeof(string)).Invoke(new object[] { f._TempFolder + src_save });

            f.get("save").Invoke(obj: instance, parameters: new Object[] { f._TempFolder + dst });

            Fact.Assert.Misc.ExpectTestPass(Tools.Tools.file_equals(f._TempFolder + src_save, f._TempFolder + dst));
        }

        [Fact.Attribute.Test.Test("BmpReader: Binarize")]
        public void bin_BMP() {

            string dst = "./flag_bin.bmp";
            string dst_ref = "./flag_bin_ref.bmp";
            BMPReader b = new BMPReader(f._TempFolder + src_bin);
            Filters.binarize(b, 125);
            b.save(f._TempFolder + dst_ref);

            Type t = f.get_class("BMPReader");
            object instance = f.get_ctor(t, typeof(string)).Invoke(new object[] { f._TempFolder + src_bin });

            f.get("binarize").Invoke(obj: null, parameters: new Object[] { instance, 125 });
            f.get("save").Invoke(obj: instance, parameters: new Object[] { f._TempFolder + dst });

            Fact.Assert.Misc.ExpectTestPass(Tools.Tools.file_equals(f._TempFolder + dst_ref, f._TempFolder + dst));
        }

        [Fact.Attribute.Test.Test("BmpReader: GreyScale")]
        public void grey_BMP() {

            string dst = "./flag_grey_scale.bmp";
            string dst_ref = "./flag_grey_scale_ref.bmp";

            BMPReader b = new BMPReader(f._TempFolder + src_grey);
            Filters.to_grey(b);
            b.save(f._TempFolder + dst_ref);

            Type t = f.get_class("BMPReader");
            object instance = f.get_ctor(t, typeof(string)).Invoke(new object[] { f._TempFolder + src_grey });

            f.get("to_grey").Invoke(obj: null, parameters: new Object[] { instance });
            f.get("save").Invoke(obj: instance, parameters: new Object[] { f._TempFolder + dst });
            Fact.Assert.Misc.ExpectTestPass(Tools.Tools.file_equals(f._TempFolder + dst_ref, f._TempFolder + dst));
        }

        [Fact.Attribute.Test.Test("BmpReader: Convolution: Sharp")]
        public void sharp_BMP() {
            float[,] sharp = new float[,] {
                { 0, -1, 0},
                { -1, 5, -1},
                { 0, -1, 0},
            };

            this.conv_BMP(sharp, "sharpen");
        }

        [Fact.Attribute.Test.Test("BmpReader: Convolution: Blur")]
        public void blur_BMP() {
            float[,] sharp = new float[,] {
                { 1, 1, 1},
                { 1, 1, 1},
                { 1, 1, 1},
            };

            this.conv_BMP(sharp, "blur");
        }


        [Fact.Attribute.Test.Test("BmpReader: Convolution: Edge")]
        public void edge_BMP() {
            float[,] sharp = new float[,] {
                { 0, 1, 0},
                { 1, -4, 1},
                { 0, 1, 0},
            };

            this.conv_BMP(sharp, "edge");
        }


        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown() {
            f.Cleanup();
        }


        public void conv_BMP(float[, ] mat, string name) {

            string dst = "./flag_conv_" + name + ".bmp";
            string dst_ref = "./flag_conv_" + name + "_ref.bmp";

            BMPReader b = new BMPReader(f._TempFolder + src_conv);
            Filters.convolution(b, mat);
            b.save(f._TempFolder + dst_ref);

            Type t = f.get_class("BMPReader");
            object instance = f.get_ctor(t, typeof(string)).Invoke(new object[] { f._TempFolder + src_conv });

            f.get("convolution").Invoke(obj: null, parameters: new Object[] { instance, mat });
            f.get("save").Invoke(obj: instance, parameters: new Object[] { f._TempFolder + dst });
            Fact.Assert.Misc.ExpectTestPass(Tools.Tools.file_equals(f._TempFolder + dst_ref, f._TempFolder + dst));
        }

        private void create_bitmap() {
            Random r = new Random();

            flag = new Bitmap(r.Next(300), r.Next(300), System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            for (int x = 0; x < flag.Width; ++x)
                for (int y = 0; y < flag.Height; ++y)
                    flag.SetPixel(x, y, Color.FromArgb(r.Next(255), r.Next(255), r.Next(255)));

            flag.Save(f._TempFolder + src_header, System.Drawing.Imaging.ImageFormat.Bmp);
            flag.Save(f._TempFolder + src_save, System.Drawing.Imaging.ImageFormat.Bmp);
            flag.Save(f._TempFolder + src_bin, System.Drawing.Imaging.ImageFormat.Bmp);
            flag.Save(f._TempFolder + src_grey, System.Drawing.Imaging.ImageFormat.Bmp);
            flag.Save(f._TempFolder + src_conv, System.Drawing.Imaging.ImageFormat.Bmp);
        }

        private String readHeader_ref() {
            BMPReader b = new BMPReader(f._TempFolder + src_header);

            TextWriter tmp = Console.Out;
            StringWriter sw = new StringWriter();

            Console.SetOut(sw);
            b.display_header();
            string result = sw.GetStringBuilder().ToString();
            Console.SetOut(tmp);

            return result;
        }
    }
}
