﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6_EX1 {
    public class Program {

        public static String read_file(string filename) {
            if (filename == "")
                return "";

            String result = "";
            try {
                using (StreamReader sr = new StreamReader(filename)) { result = sr.ReadToEnd(); }
            }
            catch (Exception e) {
                Console.WriteLine("The file could not be read: {0}", e.Message);
            }

            return result;
        }

        public static void write_to_file(string filename, string[] lines) {
            if (lines == null)
                return;

            using (StreamWriter file = new StreamWriter(filename)) {
                foreach (string line in lines)
                    file.WriteLine(line);
            }
        }
    }
}
